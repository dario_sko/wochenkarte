<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet"
    ">

    <title>Willkommen</title>
</head>
<body class="bg-dark text-white">

<?php
if (!isset($_COOKIE['active'])) {
    ?>
    <!-- Cookie Form -->
    <div class="container-fluid text-center mt-5 col-sm-5 border border-white rounded-3">
        <div class="col-sm-auto">
            <h1>Wochenkarte</h1>
            <h2>Willkommen</h2>
            <p>Diese Website verwendet Cookies</p>
        </div>

        <form action="cookie.php" method="post" id="cookie_form">
            <div class="col">
                <input type="submit" name="submit" class="btn btn-warning btn-lg w-50 mb-1" value="Akzeptieren"/>
            </div>
        </form>
    </div>

    <?php
} else {

    session_start();

    ?>

    <!-- Login Form -->
    <div class="container text-center mt-5 col-sm-5 border border-white rounded-3">

        <div class="col-sm-auto m-3">
            <h1>Wochenkarte</h1>
            <h2>Bitte anmelden</h2>
        </div>

        <?php
        require_once "models/User.php";

        $u = new User();

        if (isset($_POST['submit'])) {
            $u->login();
            header("Location: karte.php");

            if (isset($errors['login'])) {
                echo "<div class='alert alert-danger'> <p>Die eingegebenen Daten sind Fehlerhaft</p>";
                echo "<p>" . $errors['login'] . "</p>";
            }
            echo "</div>";
        }


        // Debug Session
        //        print_r($_SESSION['user'] ?? "Session nicht vorhanden")
        ?>

        <form id="login_form" method="post" action="index.php">
            <div class="col-sm-auto m-3">
                <input type="text"
                       name="name"
                       required
                       class="form-control w-100
                       <?= isset($errors['login']) ? 'is-invalid' : '' ?>"
                       value="<?= htmlspecialchars("") ?>"
                       placeholder="Name"
                       min="2"
                       max="25"
                />
            </div>

            <div class="col-sm-auto m-3">
                <input type="password"
                       name="password"
                       required
                       class="form-control w-100
                       <?= isset($errors['login']) ? 'is-invalid' : '' ?>"
                       value="<?= htmlspecialchars("") ?>"
                       placeholder="Passwort">
            </div>


            <div class="col-sm-auto m-3">
                <input type="submit" name="submit" value="Anmelden" class="btn btn-primary w-100"</input>
            </div>
        </form>
    </div>
    <?php
}
?>
</body>
</html>