<?php

class User
{
    private $name = "Clemens";
    private $password = "12345678";

    public $errors = [];


    public static function get($name, $password)
    {
        return $name & $password;
    }

    public function login()
    {
        global $errors;
        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $password = $_POST['password'];
            if ($this->name == $name && $this->password == $password) {
                $s = serialize($this); // Objektumwandlung in serialisierte Zeichenkette
                $_SESSION['user'] = $s; // Speicherung der serialisierten Zeichenkette in Session Array
                return true;
            } else {
                $errors['login'] = "Logindaten sind falsch!";
                return false;
            }
        }
    }

    public static function logout()
    {
        unset($_SESSION['user']);

    }

    public static function isLoggedIn()
    {
        if (isset($_SESSION['user'])) {
            return true;
        } else {
            return false;
        }
    }

    public  function validateName(){

        if (strlen($this->name) < 2){
            $this->errors['name'] = "Name muss mindestens zwei Zeichen haben.";
            return false;
        }else if (strlen($this->name) > 25){
            $this->errors['name'] = "Name darf nicht länger sein als 25 Zeichen.";
            return false;
        }else{
            return true;
        }
    }

    public function validatePassword(){
        if (strlen($this->password) < 8){
            $this->errors['password'] = "Das Passwort muss mindestens 8 Zeichen enthalten.";
            return false;
        }else{
            return true;
        }
    }

    public function validate(){
        return $this->validateName() & $this->validatePassword();
    }

    /**
     * @return array
     */
    public function getError()
    {
        return $this->errors;
    }

    public function hasError($field)
    {
        return isset($this->errors[$field]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }






}