<?php
session_start()
?>
<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <title>Wochenkarte</title>
</head>
<body class="bg-dark text-white">

<?php
require_once "models/User.php";
if (User::isLoggedIn()) {
    ?>
    <h1 class="text-center">Wochenkarte</h1>
    <div class="container text-center">
        <?php
        require_once "models/User.php";
        if (isset($_POST['logout'])) {
            User::logout();
            header("Location: index.php");
        }
        ?>
        <form id="logout_form" method="post" action="karte.php">
        <input type="submit" name="logout" value="Logout" class="btn btn-primary"</input>
        </form>
    </div>
    <div class="container">
        <div class="row text-center">
            <div class="col-sm-4 mt-1 mb-1 ">
                <h3 class="text-center">Montag</h3>
                <img src="image/Schnitzel.png" class="img-fluid rounded-3" style="width: 200px;height: 200px">
            </div>
            <div class="col-sm-4 mt-1 mb-1">
                <h3 class="text-center">Dienstag</h3>
                <img src="image/Steak.png" class="img-fluid rounded-3" style="width: 200px;height: 200px">
            </div>
            <div class="col-sm-4 mt-1 mb-1">
                <h3 class="text-center">Mittwoch</h3>
                <img src="image/Ravioli.png" class="img-fluid rounded-3" style="width: 200px;height: 200px">
            </div>
        </div>
        <div class="row text-center">
            <div class="col-sm-4 mt-1 mb-1">
                <h3 class="text-center">Donnerstag</h3>
                <img src="image/Pizza.png" class="img-fluid rounded-3" style="width: 200px;height: 200px">
            </div>
            <div class="col-sm-4 mt-1 mb-1">
                <h3 class="text-center">Freitag</h3>
                <img src="image/Pasta.png" class="img-fluid rounded-3" style="width: 200px;height: 200px">
            </div>
            <div class="col-sm-4 mt-1 mb-1">
                <h3 class="text-center">Samstag</h3>
                <img src="image/Lasagne.png" class="img-fluid rounded-3" style="width: 200px;height: 200px">
            </div>
        </div>
        <div class="row text-center">
            <div class="col-sm-4 mt-1 mb-1">
            </div>
            <div class="col-sm-4 mt-1 mb-1">
                <h3 class="text-center">Sonntag</h3>
                <img src="image/Fitness.png" class="img-fluid rounded-3" style="width: 200px;height: 200px">
            </div>
            <div class="col-sm-4 mt-1 mb-1">
            </div>
        </div>
    </div>

<?php } else { ?>

    <h1 class="text-center">Kein Zutritt!</h1>

<?php } ?>
</body>
</html>